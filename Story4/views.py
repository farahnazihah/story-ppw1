from django.shortcuts import render
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from datetime import datetime

# Create your views here.

def index(request):
    return render(request, "Story4/profile.html", {})

def games(request):
    return render(request, "Story4/games.html", {})

def time(request, additional = 0):
    now = datetime.now()
    hour = (now.hour + additional) % 24
    curtime = now.replace(hour=hour).strftime("%H:%M:%S")
    
    return render(request, "Story4/currenttime.html", {"time":curtime})