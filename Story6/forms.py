from django.forms import ModelForm
from Story6 import models

class kegiatanForm(ModelForm):
    
    required_css_class = 'required'
    class Meta:
        model = models.Kegiatan
        fields = ['namaKegiatan']

class orangForm(ModelForm):
    class Meta:
        model = models.Person
        fields = ['namaOrang']
